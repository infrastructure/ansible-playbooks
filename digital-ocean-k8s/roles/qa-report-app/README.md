# qa-report-app

These playbooks deploy
[qa-report-app](https://gitlab.apertis.org/infrastructure/qa-report-app).

## Updating

Ensure the GitLab pipeline has completed for the Git commit to update to, then
update `git:revision:` in [`qa-report-app.yaml`](../../qa-report-app.yaml).
