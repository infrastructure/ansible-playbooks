# ansible-playbooks

This repository contains various Ansible playbooks for Apertis infrastructure.
At the moment, they are primarily for deploying services to our Kubernetes
cluster.

### Setup

This repository bundles all the needed modules and dependencies in a OCI
container image built on GitLab CI/CD to minimize the set up effort.

To enter the container with Docker, run:

    docker run -it --rm --security-opt label=disable \
       --name ansible-playbooks-env \
       --user "$(id -u):$(id -g)" \
       -v ~/apertis-home:/home/apertis -e HOME=/home/apertis \
       -w /ansible-playbooks -v $(pwd):/ansible-playbooks \
       registry.gitlab.apertis.org/infrastructure/ansible-playbooks/env

This will:

1. mount `~/apertis-home` as `/home/apertis` to preserve credentials
   across invocations in a isolated way to minimize the chance of mistakes
2. mount the current dir as `/ansible-playbooks` and enter it
3. run as your current user rather than `root`: replace `--user` with
   `--userns keep-id` if using Podman

Check [the `Dockerfile`](docker/Dockerfile) for the list of available tools.

### Authentication

To initialize the credentials, **enter the container**, log in to the Digital
Ocean control panel and the Collabora Hashicorp Vault instance, retrieve the
tokens and set them in the container.

#### DigitalOcean Kubernetes

Get an [API token](https://docs.digitalocean.com/reference/doctl/how-to/install/#step-2-create-an-api-token)
and pass it to `doctl auth init`.

Then [get the Kubernetes config] (https://docs.digitalocean.com/products/kubernetes/how-to/connect-to-cluster/#doctl)
with `doctl kubernetes cluster kubeconfig save k8s-apertis`.

In order to confirm the cluster access has been set up properly, run
`kubectl config get-contexts`: you should see `do-ams3-k8s-apertis` listed in
the output.

To minimize the chance of errors, avoid relying on the default `kubectl`
context with `sed -i '/current-context:/d' ~/.kube/config`.

#### Hashicorp Vault

Most playbooks require access to Hashicorp Vault.

In that case, you should:

1. access [our Hashicorp Vault instance](https://vault.collabora.com:8200/)
2. choose to sign-in via OIDC specifying the `vault-apertis-admin` role
3. obtain an access token by clicking the user icon on the top left and
   selecting *Copy token*
4. set it in the current shell session via:
   ```bash
   $ export VAULT_TOKEN=THE-TOKEN-VALUE-YOU-JUST-COPIED
   ```

Note that these tokens expire after a few hours, after which this variable will
need to be set to a new token. (You can also select *Renew token* in the user
menu to renew the same token before it expires.)

### Running the Playbooks for Deployment

With the above setup in place, you can easily run a specific playbook via:

```bash
$ ansible-playbook digital-ocean-k8s/PLAYBOOK.yaml
```

If you get:

```bash
An unhandled exception occurred while running the lookup plugin 'community.hashi_vault.vault_kv2_get'. Error was a <class 'ansible.errors.AnsibleError'>, original message: Invalid Vault Token Specified.. Invalid Vault Token Specified.
```

then you need to [set up access to Vault first](#hashicorp-vault).

### Testing the Playbooks

You can run pass `--check` to `ansible-playbook` to run a playbook in
[check mode](https://docs.ansible.com/ansible/latest/user_guide/playbooks_checkmode.html):

```
$ ansible-playbook --check digital-ocean-k8s/PLAYBOOK.yaml
```

This is, in theory, a dry run that simply checks the playbook's general
validity. However, it is important to note that, **in practice, this will fully
execute certain tasks in a playbook regardless**. In particular,
`kubernetes.core.k8s_exec` will run normally even in check mode. Any tasks that
do this *should* be annotated with `when: not ansible_check_mode`, which will
fix this, but there's no particular guarantee that all instances have been
modified appropriately. Therefore, do not *assume* that `--check` will not run
any permanent modifications.

### Special Considerations

Details on updating specific services or special considerations will be found in
a README contained within the playbook's `roles` folder (e.g.
`digital-ocean-k8s/roles/PLAYBOOK/README.md`).

### Tips and tricks

* Dump the Ansible host variables
  ```
  ansible all -m debug -a 'var=hostvars'
  ```
