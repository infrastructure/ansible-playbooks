# PostgreSQL Upgrade

The migration task involves upgrading the PostgreSQL deployment to a specified version, ensuring an updated and compatible PostgreSQL setup.

## Goals:

-  Upgrade to a major version of PostgreSQL with minimal risk and downtime.
-  Repeatable.
-  Easy to automate at large scale.
-  Works on Kubernetes

## Non-goals:

-  Data validation scripts
-  Provide production ready automation scripts to run this for large scale database deployment

## How to run

ansible-playbook -e namespace=tst-qa-report-app \
    -e k8s_context=do-ams3-k8s-apertis \
    -e postgres_upgrade=14-to-15 \
    -e pg_statefulset=postgresql \
    /ansible-playbooks/digital-ocean-k8s/migrations-k8s-postgresql-upgrade.yaml

Required variables:
- namespace
- k8s_context
- postgres_upgrade 
  Available versions: https://github.com/tianon/docker-postgres-upgrade
- pg_statefulset (postgresql by default)
- pg_pvc_data (data-postgresql-0 by default)