# QA-reports-app

## Migrate postgres database

Sometimes it's necessary to conduct tests with real data from production instances, and it's essential to extract the data from the database to replicate it in development or testing environments. This guide enables exporting data from PostgreSQL and importing it into another QA-report instance.

### Required information

- Postgres User
- Postgres Password
- Postgres Database Name

### Import data from the production qa-report instance

Connect to the postgres POD running this commnad:

```kubectl exec -it pod/postgresql-0 -n qa-report -- /bin/bash```

Connect to the database

```psql -U USER -h localhost```

Generate a backup of the database

```pg_dump -U USER -h localhost DATABASE_NAME > /bitnami/postgresql/backup/DATABASE-NAME-DATE.bck```

Copy backup to our local

```kubectl cp postgresql-0:/bitnami/postgresql/backup/DATABASE-NAME-DATE.bck -n qa-report DATABASE-NAME-DATE.bck```

### Restore the data to a new qa-report instance

During the data import process, it is advisable to pause the qa-report service to avoid interference with the data dump and prevent errors.

Scale down qa-report service

```kubectl scale deployment.apps/qa-report-app -n qa-report-new --replicas=0```

Copy backup to the new qa-report instance 

```kubectl cp DATABASE-NAME-DATE.bck postgresql-0:/bitnami/postgresql/DATABASE-NAME-DATE.bck -n qa-report-new```

Acces to the shell of the container

```kubectl exec -it pod/postgresql-0 --container postgresql -n qa-report-app -- /bin/bash```

Connect to the database

```psql -U postgres```

Delete the qa-report database if exists 

```DROP DATABASE "DATABASE_NAME";```

Create database qa-report

```CREATE DATABASE "DATABASE_NAME";```

Import data

```psql -U postgres DATABASE_NAME < /bitnami/postgresql/DATABASE-NAME-DATE.bck```

Exit from the shell of the container

```exit```

Scale up qa-report-app

```kubectl scale deployment.apps/qa-report-app -n qa-report-new --replicas=1```
