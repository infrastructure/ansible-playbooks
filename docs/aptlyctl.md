# aptlyctl

## Installation

```sh
cargo install --locked --git https://github.com/collabora/aptly-rest-tools aptlyctl
~/.cargo/bin/aptlyctl --help
```

## Usage

Add the token to interact with the aptly instance.

``` sh
export APTLY_API_TOKEN=XXXXXXXXXXXXXXXX
```

### List repositories

``` sh
aptlyctl --api-token "$APTLY_API_TOKEN" \
    -u https://aptly.dev-cluster.collabora.dev/apertis/_aptly repo list
```

### Create repository

``` sh
aptlyctl --api-token "$APTLY_API_TOKEN" \
    -u <https://aptly.dev-cluster.collabora.dev/apertis/_aptly> \
    repo create \
    --component non-free \
    --distribution apertis \
    apertis:v2024dev0:non-free/default
```

### Publish repository

``` sh
aptlyctl --api-token "$APTLY_API_TOKEN"
    -u <https://repositories.apertis.org/apertis/_aptly> \
    publish create repo apertis \
    --architecture=source \
    --architecture=armhf \
    --architecture=amd64 \
    --architecture=arm64 \
    --distribution=v2024dev0 \
    --skip-contents --skip-bz2 \
    apertis:v2024dev0:development/default//development \
    apertis:v2024dev0:sdk/default//sdk \
    apertis:v2024dev0:target/default//target \
    apertis:v2024dev0:non-free/default//non-free \
    --gpg-key=XXXXXXXX
```

### Drop repository

``` sh
aptlyctl --api-token "$APTLY_API_TOKEN"
    -u <https://repositories.apertis.org/apertis/_aptly>
    repo drop apertis:v2024dev0:non-free/default
```

### Drop publish

``` sh
aptlyctl --api-token XXXXXX
    -u <https://repositories.apertis.org/apertis/_aptly>
    publish drop apertis v2024dev0:non-free
```

## Info about the Aptly API

<https://www.aptly.info/doc/api/repos/>
