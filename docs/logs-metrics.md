﻿# Logs and metrics

## Goals

The collected logs and metrics will be utilized for the following purposes:

- Monitoring and Alerting: 
  - The logs and metrics will be used to monitor the health, performance, and availability of the services and infrastructure components. 
  - Alerting mechanisms will be set up based on predefined thresholds to notify administrators of any anomalies or critical incidents.
- Troubleshooting and Debugging: 
  - The logs and metrics will serve as valuable sources of information for diagnosing and troubleshooting issues in the services and infrastructure. Administrators can analyze the collected data to identify root causes and take appropriate actions.
- Capacity Planning:
  - By analyzing the metrics data, administrators can gain insights into resource utilization patterns, identify potential bottlenecks, and plan for capacity scaling or optimization to ensure efficient resource allocation.
- Auditing and Compliance: 
  - The logs will be used for auditing purposes, ensuring compliance with security policies, regulations, and industry standards. Log data will be retained for a specified duration to meet compliance requirements. 
- Performance Analysis and Optimization: 
  - The collected metrics will enable administrators to analyze service performance over time, identify areas for optimization, and make data-driven decisions to improve overall system performance and efficiency.

## Environment Overview

**Kubernetes Clusters**

There are a total of 4 Kubernetes clusters in the environment.

**Services**

The environment hosts around 3 services running within the Kubernetes clusters.

**Cluster Administrators**

There are 3 cluster administrators responsible for managing the Kubernetes clusters and associated resources.

**Change Rate**

The environment experiences a moderate change rate, with occasional updates and deployments of services and infrastructure changes.

**Log and Metrics Volumes**

The environment not generates a significant volume of logs and metrics from the services and Kubernetes infrastructure.

**Expected Availability**

The logs and metrics collection infrastructure is expected to have reasonable uptime, ensuring acceptable levels of downtime and data loss.

## Architecture

A centralized log aggregation and metrics collection system will be deployed within the Kubernetes clusters to collect and store the logs and metrics. The system will be integrated with Grafana for visualization and analysis.

- Access Control and Security: 
  Access controls and RBAC policies ensure that only authorized cluster administrators have access to the logs and metrics data. Proper security measures, such as encryption and secure network communication, are employed to protect the integrity and confidentiality of the collected data.
- Scalability and Performance: 
  The log and metrics collection infrastructure is designed to scale horizontally to handle the increasing volumes of logs and metrics as the number of services and clusters grow.
- Log and Metrics Retention: 
  The logs and metrics are retained for a specified period, based on regulatory requirements and operational needs. The retention policy will be defined to strike a balance between storage costs and the need for historical data analysis.
- Disaster Recovery: 
  The log and metrics collection infrastructure will have appropriate backup and disaster recovery mechanisms in place to ensure data resilience and recovery in the event of failures or data loss.

The system uses the following components:

- Metrics capture: Prometheus adapters / Vector
- Logs capture: Vector
- Metrics storage: Prometheus
- Logs storage: Loki
- Reporting: Grafana
- Alerting: Alertmanager

## Deployment strategy

### Prometheus + Grafana + Alertmanager

The [Prometheus Helm Chart](https://github.com/prometheus-community/helm-charts/tree/main/charts/prometheus) is a good option to deploy the stack, it comes with a subset of the needed components already configured:

- [Prometheus](https://prometheus.io/)
- [Alertmanager](https://github.com/prometheus/alertmanager)
- [Prometheus node-exporter](https://github.com/prometheus/node_exporter)
- [Prometheus Adapter for Kubernetes Metrics APIs](https://github.com/kubernetes-sigs/prometheus-adapter)
- [kube-state-metrics](https://github.com/kubernetes/kube-state-metrics)

Pros:

- Deploys Prometheus and Alertmanager with a coherent configuration out-of-the-box

Cons:

- Since our setup is largely static, the ability of the Operator to easily create new Prometheus instances is not relevant to us and may add friction if we end up deviating from the common path. On the other hand, the assumption is that we do not need to deviate from the common path either.
- Metrics collection is done with Prometheus components and not with Vector. We may later investigate if it is worthwhile to switch or if a mixed setup is preferable.

## Vector

The centralized deployment topology provides a good balance of simplicity, stability, and control. It offers advantages similar to stream-based topologies, such as clean separation of responsibilities, without the management overhead associated with stream-based setups that involve using Vector alongside systems like Apache Kafka or Apache Pulsar. The [official Helm chart](https://github.com/vectordotdev/helm-charts/tree/develop/charts/vector) provides a good way to deploy it. We may later evaluate the [Vector Operator](https://github.com/kaasops/vector-operator) to provide the ability to define custom pipelines via service-specific namespaced CRDs, just like the Prometheus operator does for Prometheus rules and Alertmanager configurations, but at the moment the operator does not handle the aggregator setup yet.

Pros:

1. Efficiency: Centralized topologies are typically more efficient for client nodes and downstream services. Vector agents perform less work, resulting in lower resource usage. The centralized Vector service buffers data, applies better compression, and optimizes requests sent downstream.
1. Reliability: Vector protects downstream services from volume spikes by buffering and flushing data at regular intervals.
1. Multi-host context: With centralized data, operations can be performed across hosts, enabling the reduction of logs to global metrics. This is beneficial for large deployments where aggregated metrics across multiple hosts provide more informative insights than isolated per-host metrics.

Cons:

1. Complexity: A centralized topology involves more components to manage as Vector needs to be run in both the agent and aggregator roles.
1. Durability: Agent nodes prioritize quickly offloading data from the machine, which may lead to potential data loss if the central Vector service goes down, resulting in the loss of buffered data.

## Grafana Loki

The Helm Chart allow to install Loki cluster in two modes, [monolithic](https://grafana.com/docs/loki/latest/installation/helm/install-monolithic/) and [scalable](https://grafana.com/docs/loki/latest/installation/helm/install-scalable/).

Loki is designed to easily redeploy a cluster under a different mode as the needs change, with no configuration changes or minimal configuration changes. The Helm chart defaults to the scalable mode, which seems better suited to a Kubernetes environment.

# Deployment Tasks

Introduction

The purpose of this report is to provide a comprehensive guide for deploying Vector, Prometheus, Grafana and Loki in a Kubernetes cluster. This deployment will enable efficient monitoring and log aggregation within your cluster. The report outlines the necessary tasks involved in deploying and configuring these tools to ensure a successful implementation.

Deployment Steps:

- Deploy Prometheus:
  - Deploy the prometheus using the [Helm Chart](https://github.com/prometheus-community/helm-charts/tree/main/charts/prometheus) 
  - Customize the configuration based on monitoring requirements.
  - Ensure the Prometheus server, Alertmanager, and other components are running successfully.
- Deploy Grafana:
  - Verify the availability of the Grafana UI.
- Deploy Loki:
  - Deploy Loki using the [Helm chart](https://github.com/grafana/loki/tree/main/production/helm/loki).
  - Configure the desired retention period, authentication, and other settings.
  - Verify the successful deployment and availability of Loki Grafana.
- Deploy Vector:
  - Deploy Vector using the [Helm chart](https://github.com/vectordotdev/helm-charts/tree/develop/charts/vector).
  - Configure Vector to collect logs from all pods
  - Configure Vector to submit logs to Loki
  - Verify the successful deployment of Vector and log collection.
- Integration of Components: Once the individual components are deployed, integrate them to achieve a comprehensive logging and monitoring solution:
  - Ensure that logs from Pods are captured by Vector and stored in Loki
  - Explore the logs and metrics in Grafana by creating dashboards and alerts.
  - Ensure the connectivity and data flow between Vector, Loki, Prometheus, and Grafana.


