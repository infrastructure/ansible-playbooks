# Aptly role

The aptly role will deploy an Aptly API server combined with a small webserver
to serve the published repositories. The API server is currently authenticated
with a token, the repository server is public

## Requirements before deployment

* A token generate for aplty auth (e.g. via `makepasswd  --chars=32`) and
  registered in the vault as secret `<prefix>/api` with key `token`
* A gpg key generated for signing and register in the fault as `<prefix>/gpg`
  with the key `key` for the private key and key `fingerprint` for the token
  fingerprint (as hex string without spaces)

## gnupg key generation

Debian generally seems to use rsa4096 as the algorithm for it's gnupg keys. At
the time of this writing only the bookworm stable key is ed25519, but even the
bookworm security key is rsa4096. As such for optimal compatibility currently
rsa4096 is recommended.

To generate a key the following steps can be used:
```
$ export GNUPGHOME=aptly
$ mkdir ${GNUPGHOME}
# Change the name and expiry date to fit
$ gpg  --quick-generate-key --batch --passphrase '' "Example Key Name" rsa4096 default 2028-06-20
# Fingerprint for the key name, key id is the last 8 hex numbers
$ gpg --fingerprint
# Output the private key block to stick in the vault
$ gpg --export-secret-key --armor
```

